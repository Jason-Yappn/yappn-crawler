
## Requirements

`shub` utility is required to work with Scrapy Cloud: to deploy new version of the project or to locally schedule  spiders on Dash.
 See its documentation here: [doc.scrapinghub.com/shub.html](http://doc.scrapinghub.com/shub.html).

## Get the code

Spiders project code is at bitbucket: https://bitbucket.org/scrapinghub/yappn-crawler/. You can clone (download) it with `git clone git@bitbucket.org:scrapinghub/yappn-crawler.git`

## Description

Project contains one spider: yappn_crawler: https://bitbucket.org/scrapinghub/yappn-crawler/src/901d63847e42b22b80d62d5f13a0011680ea6e2a/yappn/spiders/yappn_crawler.py?at=master&fileviewer=file-view-default.

Spider is designed to take input arguments in two ways:

    1. single crawl mode. When you can set up arguments for one url. List of arguments:
        - language
        - source_url
        - crawl_limit
        - use_splash
        - infinite_scroll_depth
        - custom_cookie
        - custom_session_var
        - custom_query_string

    2. multi crawl mode. When you can set up different combinations of specifications from single crawl. Arguments: 
        - input_file (direct download link to xlsx file, with combination of specs per line)

Example of xlsx file located here: `https://bitbucket.org/scrapinghub/yappn-crawler/src/e9457ef1ee63cb14d46359cc7fcbe324a1757678/sample_change_language.xlsx?at=master&fileviewer=file-view-default`

Spider also will try to find sitemap on the page and start scraping results from there.

With arguments: custom_cookie, custom_session_var and custom_query_string  you can change the language of the site.
For example:
`custom_cookie={"userLanguage":"en"}`
`custom_query_string=?setLang=en`

## Running spiders

You can run companies spider locally with `scrapy crawl yappn`. Otherwise you can schedule it on Dash `https://app.scrapinghub.com/p/117333/jobs`
To run spider locally with arguments use -a option.
For example `scrapy crawl yappn -a source_url="https://zh.zalora.com.hk" -a custom_query="?setLang=en"`

To run spiders on Dash you should deploy the project first (already done by scrpinghub development team). `shub deploy prod` is used to deploy current code to production project on a Scrapy Cloud.


### How to run spiders in Dash

    - click on `Run` button
    - choose the spider
    - fill in arguments
