# -*- coding: utf-8 -*-


from scrapy import Item, Field
from scrapy.loader import ItemLoader
from scrapy.loader.processors import Identity, TakeFirst


class YappnItem(Item):
    language = Field()
    source_url = Field()
    crawl_limit = Field()
    use_splash = Field()
    infinite_scroll_depth = Field()
    custom_cookie = Field()
    custom_session_var = Field()
    custom_query_string = Field()
    page_url = Field()
    text = Field(output_processor=Identity())


class YappnItemLoader(ItemLoader):
    default_item_class = YappnItem
    default_output_processor = TakeFirst()
