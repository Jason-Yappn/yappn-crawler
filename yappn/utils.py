# -*- coding: utf-8 -*-

import csv
import urllib
import xlrd


def get_configs_from_csv(url):
    filename = 'items.csv'
    urllib.urlretrieve(url, filename)
    csv_iter = csv.reader(file(filename))
    headers = next(csv_iter)[0].split(';')
    rows = [row[0].split(';') for row in csv_iter]

    configs = []

    for row in rows:
        configs.append(dict(zip(headers, row)))

    return configs


def get_configs_from_xlsx(url):
    filename = 'items.xlxs'
    urllib.urlretrieve(url, filename)
    worksheet = xlrd.open_workbook(filename).sheet_by_index(0)
    rows = []
    configs = []

    for i, row in enumerate(range(worksheet.nrows)):
        r = []

        for j, col in enumerate(range(worksheet.ncols)):
            r.append(worksheet.cell_value(i, j))

        rows.append(r)

    headers = rows[0]

    for row in rows[1:]:
        configs.append(dict(zip(headers, row)))

    return configs
