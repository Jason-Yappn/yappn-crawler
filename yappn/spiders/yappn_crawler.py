# -*- coding: utf-8 -*-

import json

from urlparse import urljoin
from scrapy_splash import SplashRequest
from scrapy.spiders import Spider, Request
from scrapy.linkextractors import LinkExtractor
from yappn.items import YappnItemLoader, YappnItem
from yappn.utils import get_configs_from_xlsx
from yappn.settings import SPLASH_USER


class YappnSpider(Spider):
    name = "yappn"
    single_crawl = True
    http_user = SPLASH_USER
    http_pass = ''

    page_count_per_domain = {}

    # set default priority to requests from documentation:
    # Requests with a higher priority value will execute earlier.
    # Negative values are allowed in order to indicate relatively low-priority.

    priority = 1

    def __init__(self, *args, **kwargs):
        super(YappnSpider, self).__init__(*args, **kwargs)

        ''' Set arguments for single and multi crawl:
        for multy_crawl:
         - input_file

        for single_crawl:
         - language
         - source_url
         - crawl_limit
         - use_splash
         - infinite_scroll_depth
         - custom_cookie
         - custom_session_var
         - custom_query_string
         '''

        if kwargs.get('input_file'):
            self.single_crawl = False

        if self.single_crawl:
            self.configs = [kwargs]
        else:
            # get list of configs from csv file
            self.configs = get_configs_from_xlsx(kwargs.get('input_file'))

        self.start_urls = [config['source_url'] for config in self.configs]
        self.infinite_scroll_lua_script = """
            function main(splash)
                splash:autoload("https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js")
                assert(splash:go(splash.args.url))
                splash:wait(1)

                for var=1,splash.args.infinite_scroll_depth do
                    assert(splash:runjs("window.scrollTo(0, document.body.scrollHeight);"))
                    splash:wait(3)
                end

                splash:set_result_content_type("text/html; charset=utf-8")
                return splash:html()
            end
            """
        self.base_lua_script = """
            function main(splash)
                local ok, reason = splash:go{
                    url=splash.args.url}
                if #(splash:history()) == 0 then
                    error(reason)
                end
                splash:wait(1)

                return splash:html()
            end
            """

    def start_requests(self):
        requests = []

        for url in self.start_urls:
            config = (c for c in self.configs if c['source_url'] == url).next()
            config['domain'] = url.split('/')[2]
            self.page_count_per_domain[config['domain']] = 0

            config['custom_query_string'] = config.get('custom_query_string', None)
            final_url = urljoin(url, config.get('custom_query_string'))

            if config.get('use_splash'):
                request = SplashRequest(
                    final_url,
                    callback=self.check_for_sitemap,
                    endpoint='execute',
                    args={'lua_source': self.base_lua_script,
                          'timeout': 60,
                          'url': final_url
                          },
                    meta={'config': config}
                )
            else:
                request = Request(
                    urljoin(url, config.get('custom_query_string')),
                    callback=self.check_for_sitemap,
                    meta={'config': config}
                )

            if config.get('custom_cookie'):
                cookies = {}

                try:
                    cookies = json.loads(config.get('custom_cookie'))
                    self.logger.debug('custom cookies is %s' % cookies)
                except:
                    pass

                if config.get('custom_session_var'):
                    cookies['session'] = config.get('custom_session_var')

                if cookies:
                    request.cookies = cookies

            requests.append(request)

        return requests

    def check_for_sitemap(self, response):
        config = response.meta.copy()['config']
        sitemap_link = response.xpath('//*[contains(@href, "sitemap")]/@href').extract_first()
        if sitemap_link:
            self.logger.debug('Found sitemap on page %s' % response.url)

        url = response.urljoin(sitemap_link) if sitemap_link else response.url

        if config.get('use_splash'):
            request = SplashRequest(url,
                                    endpoint='execute',
                                    args={'lua_source': self.base_lua_script,
                                          'timeout': 60,
                                          'url': url
                                          },
                                    meta={'config': config},
                                    dont_filter=True)
        else:
            request = Request(url, meta={'config': config}, dont_filter=True)

        yield request

    def parse(self, response):
        self.logger.debug('links left in queue %s' % len(self.crawler.engine.slot.scheduler))
        config = response.meta.copy()['config']

        il = YappnItemLoader(response=response)
        text = [i for i in self.get_text_nodes(response) if len(i) > 1 or i.isalnum()]

        il.add_value('text', text)
        il.add_value('page_url', response.url)

        for field in YappnItem.fields:
            il.add_value(field, config.get(field))

            if not il.get_output_value(field):
                il.add_value(field, ' ')

        item = il.load_item()

        # check if need to make infinite scroll
        continue_infinite_scroll = self.continue_infinite_scroll(response.body)
        self.logger.debug('continue %s' % continue_infinite_scroll)

        if config.get('use_splash') and continue_infinite_scroll:
            try:
                scroll_depth = int(config.get('infinite_scroll_depth'))
            except:
                scroll_depth = 1

            yield SplashRequest(
                response.url,
                callback=self.parse_after_scroll,
                endpoint='execute',
                args={'lua_source': self.infinite_scroll_lua_script,
                      'timeout': 60,
                      'infinite_scroll_depth': scroll_depth,
                      'url': response.url,
                      },
                meta={'config': config, 'item': item},
                dont_filter=True
            )

            return

        yield item

        li = LinkExtractor(restrict_xpaths=['//a'], allow_domains=config['domain'])

        # get links according to crawl limit
        links_to_crawl = self.get_links_for_next_request(
            config['domain'],
            config.get('crawl_limit'),
            [link.url for link in li.extract_links(response)]
        )

        self.page_count_per_domain[config['domain']] += len(links_to_crawl)
        self.priority += 1

        for link in links_to_crawl:
            if config.get('use_splash'):
                request = SplashRequest(
                    link,
                    endpoint='execute',
                    args={'lua_source': self.base_lua_script,
                          'timeout': 60,
                          'url': link},
                    meta={'config': config},
                    priority=-self.priority
                )
            else:
                request = Request(link, meta={'config': config}, priority=-self.priority)

            yield request

    def parse_after_scroll(self, response):
        il = YappnItemLoader(item=response.meta['item'])
        text = [i for i in self.get_text_nodes(response) if len(i) > 1 or i.isalnum()]
        il.add_value('text', text)

        yield il.load_item()

    def get_text_nodes(self, response):
        return [s.strip()
                for s in response.xpath(
                        '//body//*[not(self::a or self::script or self::style)'
                        ' and self::p or self::div or self::span or self::li]'
                        '/text()[normalize-space()]').extract()
                if s.strip()]

    def continue_infinite_scroll(self, body):
        if "(window).on('scroll', function()" in body:
            return True

    def parse_sitemap(self, response):
        config = response.meta.copy()['config']
        response.selector.remove_namespaces()
        links_to_crawl = self.get_links_for_next_request(
            config['domain'],
            config['crawl_limit'],
            response.xpath('//loc/text()').extract()
        )

        for link in links_to_crawl:
            if config.get('use_splash'):
                request = SplashRequest(
                    link,
                    endpoint='execute',
                    args={'lua_source': self.base_lua_script,
                          'timeout': 60,
                          'url': link},
                    meta={'config': config})
            else:
                request = Request(link, meta={'config': config})

            yield request

    def get_links_for_next_request(self, domain, crawl_limit, links_extracted):
        '''Returns list of links according to crawl limit and number of links already scraped'''
        if crawl_limit:
            num_links_left_to_crawl = int(crawl_limit) - self.page_count_per_domain[domain]
        else:
            num_links_left_to_crawl = len(links_extracted)

        self.logger.debug('num_links_left_to_crawl %s' % num_links_left_to_crawl)

        if len(links_extracted) >= num_links_left_to_crawl:
            links_to_crawl = links_extracted[:num_links_left_to_crawl]
        else:
            links_to_crawl = links_extracted[:]

        return links_to_crawl
